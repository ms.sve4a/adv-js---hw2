class Hamburger {
    constructor (size, stuffing) {
        this._stuffings = [];
        this._toppings = [];
        this.addSize(size);
        this.addStuffing(stuffing);
    }

    addSize (size){
        try {
            if(!size || size.type !== "size"){
                throw new HamburgerException("Wrong type of size")
            }
            this._size = size;
        } catch(err) {
            console.error(err);
        }
    }

    addStuffing (stuffing){
        try {
            if(!stuffing || stuffing.type !== "stuffing") {
                throw new HamburgerException("Wrong type of stuffing")
            }
            this._stuffings.push(stuffing);
        } catch(err) {
            console.error(err);
        }
    }

    addTopping (topping){
        try {
            if(!topping || topping.type !== "topping") {
                throw new HamburgerException("Wrong type of topping")
            }
            this._toppings.push(topping);
        } catch(err) {
            console.error(err);
        }
    }

    removeTopping (topping){
        try {
            if(!topping || topping.type !== "topping") {
                throw new HamburgerException("Wrong type of topping")
            }
            let index = this._toppings.indexOf(topping);
            if (index > -1) {
                this._toppings.splice(index, 1);
            }
        } catch(err) {
            console.error(err);
        }
    }

    getToppings (){
        return this._toppings;
    }

    getSize () {
        return this._size.size;
    }

    getStuffing () {
        return this._stuffings;
    }

    calculatePrice (){
        let price = 0;
        price += this._size.price;
        price += this._stuffings.reduce(function(memo, stuffing){ return memo + stuffing.price },0);
        price += this._toppings.reduce(function(memo, topping){ return memo + topping.price },0);
        return price;
    }

    calculateCalories (){
        let calories = 0;
        calories += this._size.calories;
        calories += this._stuffings.reduce(function(memo, stuffing){ return memo + stuffing.calories },0);
        calories += this._toppings.reduce(function(memo, topping){ return memo + topping.calories },0);
        return calories;
    }

    static SIZE_SMALL = {
        price: 50,
        calories: 20,
        type: "size" ,
        size: "small" ,
    };

    static SIZE_LARGE = {
        price: 100,
        calories: 40,
        type: "size" ,
        size: "large" ,
    };

    static STUFFING_CHEESE = {
        price: 10,
        calories: 20,
        type: "stuffing" ,
        stuffing: "cheese" ,
    };

    static STUFFING_SALAD = {
        price: 20,
        calories: 5,
        type: "stuffing" ,
        stuffing: "salad" ,
    };

    static STUFFING_POTATO = {
        price: 15,
        calories: 10,
        type: "stuffing" ,
        stuffing: "potato" ,
    };


    static TOPPING_MAYO = {
        price: 20,
        calories: 5,
        type: "topping" ,
        topping: "mayo" ,
    };

    static TOPPING_SPICE = {
        price: 15,
        calories: 0,
        type: "topping" ,
        topping: "spice" ,
    };

}


class HamburgerException extends Error {}


// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1



// не передали обязательные параметры
var h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'